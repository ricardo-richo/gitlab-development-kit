#!/usr/bin/env ruby
#
# frozen_string_literal: true

require_relative '../../lib/gdk'

class SetupWorkspace
  ROOT_DIR = '/projects/gitlab-development-kit'
  GDK_SETUP_FLAG_FILE = "#{ROOT_DIR}/.cache/.gdk_setup_complete".freeze

  def run
    unless gitlab_workspace_context?
      GDK::Output.info(%(Nothing to do as we're not a GitLab Workspace.\n\n))
      return
    end

    if bootstrap_needed?
      success, duration = execute_bootstrap

      create_flag_file if success
      configure_telemetry

      return unless allow_sending_telemetry?

      send_telemetry(success, duration)
    else
      GDK::Output.info("#{GDK_SETUP_FLAG_FILE} exists, GDK has already been bootstrapped.\n\nRemove the #{GDK_SETUP_FLAG_FILE} to re-bootstrap.")
    end
  end

  private

  def gitlab_workspace_context?
    ENV.key?('GL_WORKSPACE_DOMAIN_TEMPLATE') && Dir.exist?(ROOT_DIR)
  end

  def bootstrap_needed?
    !File.exist?(GDK_SETUP_FLAG_FILE)
  end

  def execute_bootstrap
    start = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    success = Shellout.new('support/gitlab-remote-development/remote-development-gdk-bootstrap.sh', chdir: ROOT_DIR).execute
    duration = Process.clock_gettime(Process::CLOCK_MONOTONIC) - start

    [success, duration]
  end

  def allow_sending_telemetry?
    GDK.config.telemetry.enabled
  end

  def configure_telemetry
    username = GDK::Output.prompt(GDK::Telemetry::PROMPT_TEXT)
    GDK::Telemetry.update_settings(username)
  end

  def send_telemetry(success, duration)
    GDK::Telemetry.send_telemetry(success, 'setup-workspace', { duration: duration, platform: 'remote-development' })
    GDK::Telemetry.flush_events
  end

  def create_flag_file
    FileUtils.mkdir_p(File.dirname(GDK_SETUP_FLAG_FILE))
    FileUtils.touch(GDK_SETUP_FLAG_FILE)
  end
end

SetupWorkspace.new.run if $PROGRAM_NAME == __FILE__
