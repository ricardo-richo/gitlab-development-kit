# Cells

## Running with Cells services

The Cells services are under active development.

Please refer to <https://gitlab.com/gitlab-org/cells/http-router/-/blob/main/README.md> for more information.

### Enabling the HTTP router

You can configure the [HTTP router](https://gitlab.com/gitlab-org/cells/http-router)
to run locally in GDK.
The HTTP router sits in front of Workhorse, and NGINX (if enabled).

To configure:

1. Set up a [local interface](local_network.md#local-interface)
1. Run `gdk config set gitlab_http_router.enabled true`
1. Run `gdk reconfigure`
1. Run `gdk start gitlab-http-router`

The router service currently supports HTTP and HTTPS.
`relative_url_root` is not supported.

By default, the HTTP router listens to the GDK port (which is by default port `3000`). You can
configure the router to use a distinct port with the following config:

```shell
gdk config set gitlab_http_router.use_distinct_port true`
gdk config set gitlab_http_router.port 9393`
```

## Install another GDK to act as a cell using support/cells-add-secondary (deprecated)

The script `support/cells-add-secondary` was
[removed](https://gitlab.com/gitlab-org/gitlab-development-kit/-/merge_requests/3617)
in GitLab 17.0.

If you previously installed a GDK using this method, follow the instructions
below to clean up.

### Clean up the installation

To clean up the installation and remove the second cell:

1. Go to the directory of the second GDK. In this example, the directory is named `gdk2`.

   ```shell
   cd ../gdk2
   ```

1. Stop the GDK for the second cell:

   ```shell
   gdk stop
   ```

1. Optional. Remove the second GDK directory:

   ```shell
   cd ..
   rm -rf gdk2
   ```
