# frozen_string_literal: true

RSpec.describe GDK::Services::GitlabHttpRouter do
  describe '#name' do
    it { expect(subject.name).to eq('gitlab-http-router') }
  end

  describe '#command' do
    let(:key_path) { Pathname.new('localhost.key').expand_path }
    let(:cert_path) { Pathname.new('localhost.crt').expand_path }
    let(:base_command) { 'support/exec-cd gitlab-http-router npx wrangler dev -e dev' }
    let(:https_args) { "--local-protocol https --https-key-path #{key_path} --https-cert-path #{cert_path}" }

    it 'returns the necessary command to run gitlab-http-router' do
      expect(subject.command).to eq("#{base_command} --port 3000 --var GITLAB_PROXY_HOST:127.0.0.1:3000")
    end

    context 'when enabled' do
      before do
        config = {
          'gitlab_http_router' => {
            'enabled' => true
          }
        }

        stub_gdk_yaml(config)
      end

      it 'returns the command with the workhorse address' do
        expect(subject.command).to eq("#{base_command} --port 3000 --var GITLAB_PROXY_HOST:127.0.0.1:3333")
      end

      context 'when nginx is enabled' do
        before do
          config = {
            'gitlab_http_router' => {
              'enabled' => true
            },
            'nginx' => {
              'enabled' => true
            }
          }

          stub_gdk_yaml(config)
        end

        it 'returns the command with the nginx address' do
          expect(subject.command).to eq("#{base_command} --port 3000 --var GITLAB_PROXY_HOST:127.0.0.1:8080")
        end
      end

      context 'when use_distinct_port' do
        before do
          config = {
            'gitlab_http_router' => {
              'enabled' => true,
              'use_distinct_port' => true
            }
          }

          stub_gdk_yaml(config)
        end

        it 'returns the command using the distinct port' do
          expect(subject.command).to eq("#{base_command} --port 9393 --var GITLAB_PROXY_HOST:127.0.0.1:3333")
        end
      end

      context 'when `https` enabled' do
        before do
          config = {
            'port' => 3443,
            'https' => {
              'enabled' => true
            },
            'gitlab_http_router' => {
              'enabled' => true
            },
            'nginx' => {
              'enabled' => true
            }
          }

          stub_gdk_yaml(config)
        end

        it 'returns the command with the nginx address' do
          expect(subject.command).to eq("#{base_command} --port 3443 --var GITLAB_PROXY_HOST:127.0.0.1:8080 #{https_args}")
        end
      end
    end
  end

  describe '#ready_message' do
    it 'returns the default ready message' do
      expect(subject.ready_message).to eq('The HTTP Router is available at http://127.0.0.1:3000.')
    end
  end

  describe '#enabled?' do
    it 'is disabled by default' do
      expect(subject.enabled?).to be(false)
    end
  end

  describe '#env' do
    it 'is empty by default' do
      expect(subject.env).to eq({})
    end

    context 'when `https` enabled' do
      before do
        config = {
          'https' => {
            'enabled' => true
          }
        }

        stub_gdk_yaml(config)
      end

      it 'returns environment variables' do
        expect(subject.env).to eq({ NODE_EXTRA_CA_CERTS: '$(mkcert -CAROOT)/rootCA.pem' })
      end
    end
  end
end
