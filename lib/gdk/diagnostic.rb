# frozen_string_literal: true

module GDK
  module Diagnostic
    def self.all
      klasses = %i[
        Environment
        RvmAndAsdf
        MacPorts
        Bundler
        Version
        Configuration
        Dependencies
        PendingMigrations
        Postgresql
        Pguser
        Geo
        Praefect
        Gitaly
        Gitlab
        Status
        Re2
        Golang
        StaleData
        StaleServices
        Chromedriver
        FileWatches
        Hostname
        Nginx
      ]

      klasses.map do |const|
        const_get(const).new
      end
    end
  end
end
