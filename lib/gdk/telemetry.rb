# frozen_string_literal: true

autoload :GitlabSDK, 'gitlab-sdk'
autoload :Sentry, 'sentry-ruby'
autoload :SnowplowTracker, 'snowplow-tracker'

module GDK
  module Telemetry
    ANALYTICS_APP_ID = 'e2e967c0-785f-40ae-9b45-5a05f729a27f'
    ANALYTICS_BASE_URL = 'https://collector.prod-1.gl-product-analytics.com'
    SENTRY_DSN = 'https://glet_d59ce9a0092b7f3e0dcecc954e4d1666@observe.gitlab.com:443/errortracking/api/v1/projects/74823'
    PROMPT_TEXT = <<-TEXT
      To improve GDK, GitLab would like to collect basic error and usage data. Please choose one of the following options:

      - To send data to GitLab, enter your GitLab username.
      - To send data to GitLab anonymously, leave blank.
      - To avoid sending data to GitLab, enter a period ('.').
    TEXT

    def self.with_telemetry(command)
      return yield unless telemetry_enabled?

      start = Process.clock_gettime(Process::CLOCK_MONOTONIC)

      result = yield

      duration = Process.clock_gettime(Process::CLOCK_MONOTONIC) - start

      send_telemetry(result, command, { duration: duration, platform: platform })

      result
    end

    def self.send_telemetry(success, command, payload = {})
      # This is tightly coupled to GDK commands and returns false when the system call exits with a non-zero status.
      status = success ? 'Finish' : 'Failed'

      client.identify(GDK.config.telemetry.username)
      client.track("#{status} #{command} #{ARGV}", payload)
    end

    def self.flush_events(async: false)
      client.flush_events(async: async)
    end

    def self.platform
      GDK.config.telemetry.platform
    end

    def self.client
      return @client if @client

      app_id = ENV.fetch('GITLAB_SDK_APP_ID', ANALYTICS_APP_ID)
      host = ENV.fetch('GITLAB_SDK_HOST', ANALYTICS_BASE_URL)

      SnowplowTracker::LOGGER.level = Logger::WARN
      @client = GitlabSDK::Client.new(app_id: app_id, host: host)
    end

    def self.init_sentry
      Sentry.init do |config|
        config.dsn = SENTRY_DSN
        config.breadcrumbs_logger = [:sentry_logger]
        config.traces_sample_rate = 1.0
        config.logger.level = Logger::WARN

        config.before_send = lambda do |event, hint|
          exception = hint[:exception]

          # Workaround for using fingerprint to make certain errors distinct.
          # See https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2842#note_1927103517
          event.transaction = exception.message if exception.is_a?(Shellout::ShelloutBaseError)

          event
        end
      end

      Sentry.set_user(username: GDK.config.telemetry.username)
    end

    def self.capture_exception(message)
      return unless telemetry_enabled?

      if message.is_a?(Exception)
        exception = message.dup
      else
        exception = StandardError.new(message)
        exception.set_backtrace(caller)
      end

      # Drop the caller GDK::Telemetry.capture_exception to make errors distinct.
      exception.set_backtrace(exception.backtrace.drop(1)) if exception.backtrace

      init_sentry
      Sentry.capture_exception(exception)
    end

    def self.telemetry_enabled?
      GDK.config.telemetry.enabled
    end

    def self.update_settings(username)
      enabled = true
      username = username.to_s

      if username.empty?
        username = SecureRandom.hex
      elsif username == '.'
        username = ''
        enabled = false
      end

      # We need to reload `gdk.yml` because it could have been modified in a separate GDK process
      # leaving the current GDK config in-memory stale.
      # See https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/2116
      GDK.config.reload
      GDK.config.bury!('telemetry.enabled', enabled)
      GDK.config.bury!('telemetry.username', username)
      GDK.config.save_yaml!
    end
  end
end
