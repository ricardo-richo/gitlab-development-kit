# frozen_string_literal: true

module GDK
  # GDK Commands
  module Command
    # This is a list of existing supported commands and their associated
    # implementation class
    COMMANDS = {
      'cleanup' => -> { GDK::Command::Cleanup },
      'clickhouse' => -> { GDK::Command::Clickhouse },
      'config' => -> { GDK::Command::Config },
      'debug-info' => -> { GDK::Command::DebugInfo },
      'diff-config' => -> { GDK::Command::DiffConfig },
      'doctor' => -> { GDK::Command::Doctor },
      'env' => -> { GDK::Command::Env },
      'install' => -> { GDK::Command::Install },
      'kill' => -> { GDK::Command::Kill },
      'help' => -> { GDK::Command::Help },
      '-help' => -> { GDK::Command::Help },
      '--help' => -> { GDK::Command::Help },
      '-h' => -> { GDK::Command::Help },
      nil => -> { GDK::Command::Help },
      'measure' => -> { GDK::Command::MeasureUrl },
      'measure-workflow' => -> { GDK::Command::MeasureWorkflow },
      'open' => -> { GDK::Command::Open },
      'telemetry' => -> { GDK::Command::Telemetry },
      'psql' => -> { GDK::Command::Psql },
      'psql-geo' => -> { GDK::Command::PsqlGeo },
      'pristine' => -> { GDK::Command::Pristine },
      'rails' => -> { GDK::Command::Rails },
      'reconfigure' => -> { GDK::Command::Reconfigure },
      'redis-cli' => -> { GDK::Command::RedisCli },
      'reset-data' => -> { GDK::Command::ResetData },
      'reset-praefect-data' => -> { GDK::Command::ResetPraefectData },
      'restart' => -> { GDK::Command::Restart },
      'run' => -> { GDK::Command::Run },
      'start' => -> { GDK::Command::Start },
      'status' => -> { GDK::Command::Status },
      'stop' => -> { GDK::Command::Stop },
      'tail' => -> { GDK::Command::Tail },
      'thin' => -> { GDK::Command::Thin },
      'trust' => -> { GDK::Command::Trust },
      'truncate-legacy-tables' => -> { GDK::Command::TruncateLegacyTables },
      'update' => -> { GDK::Command::Update },
      'version' => -> { GDK::Command::Version },
      '-version' => -> { GDK::Command::Version },
      '--version' => -> { GDK::Command::Version }
    }.freeze
  end
end
