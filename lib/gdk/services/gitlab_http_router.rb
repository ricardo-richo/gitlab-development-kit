# frozen_string_literal: true

require 'pathname'

module GDK
  module Services
    class GitlabHttpRouter < Base
      def name
        'gitlab-http-router'
      end

      def command
        base_command = "support/exec-cd gitlab-http-router npx wrangler dev -e dev --port #{active_port} --var GITLAB_PROXY_HOST:#{gitlab_host}"

        return base_command unless config.https?

        base_command << " --local-protocol https --https-key-path #{key_path} --https-cert-path #{certificate_path}"
      end

      def ready_message
        "The HTTP Router is available at #{listen_address}."
      end

      def enabled?
        config.gitlab_http_router.enabled?
      end

      def env
        return super unless config.https?

        {
          NODE_EXTRA_CA_CERTS: "$(mkcert -CAROOT)/rootCA.pem"
        }
      end

      private

      def protocol
        config.https? ? :https : :http
      end

      def listen_address
        klass = config.https? ? URI::HTTPS : URI::HTTP

        klass.build(host: config.hostname, port: active_port)
      end

      def key_path
        Pathname.new(File.basename(config.nginx.ssl.key)).expand_path
      end

      def certificate_path
        Pathname.new(File.basename(config.nginx.ssl.certificate)).expand_path
      end

      def active_port
        if config.gitlab_http_router.use_distinct_port?
          config.gitlab_http_router.port
        else
          config.port
        end
      end

      def gitlab_host
        if config.nginx?
          config.nginx.__listen_address
        else
          config.workhorse.__listen_address
        end
      end
    end
  end
end
